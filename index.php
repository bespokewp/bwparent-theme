<?php get_header(); ?>

	<main>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                        <h1><?php the_title(); ?></h1>
						<?php the_content(); ?>

					<?php endwhile; endif; ?>


                    <?php

                    if ( check_post_count( 'bwp_portfolio' ) > 0 )
                        render_portfolio(); ?>
				</div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>