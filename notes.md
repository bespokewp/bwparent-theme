#Standards

## add_fields()/add_tab() option hooks

- prefix on the hook is always `bwp_`
- format of the hooks are `bwp_modify_{fields_or_tab_identifier}_options`