////
// Dependencies
////
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');


////
// Initial tasks (after npm install is successful)
////
gulp.task('project-start', function() {

    // move font files from @fortawesome package to usable folder
    gulp.src('node_modules/@fortawesome/fontawesome-free/webfonts/*')
        .pipe(gulp.dest('assets/frontend/webfonts'));

});


////
// Frontend styles
////
gulp.task('frontend-styles', function() {

    return sass('style.scss', { style: 'compressed' })
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('./'))
        .pipe(notify({ message: 'Styles task complete' }));

});


////
// Admin styles
////
gulp.task('admin-styles', function() {

    return sass('resources/admin/scss/admin.scss', { style: 'compressed' })
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('assets/admin/css'))
        .pipe(notify({ message: 'Styles task complete' }));

});


////
// Libraries styles
////
gulp.task('libraries', function() {

    return sass('resources/frontend/libraries/scss/libraries.scss', { style: 'expanded' })
        .pipe(autoprefixer('last 2 version'))
        .pipe(gulp.dest('assets/frontend/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssnano())
        .pipe(gulp.dest('assets/frontend/css'))
        .pipe(notify({ message: 'Styles task complete' }));

});


////
// Scripts
////
gulp.task('scripts', function() {

    // make sure these are added in order of dependency
    return gulp.src([
        'node_modules/slick-carousel/slick/slick.min.js',
        'resources/frontend/theme/js/scripts.js'
    ])
        .pipe(jshint.reporter('default'))
        .pipe(rename('scripts.min.js'))
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('assets/frontend/js'))
        .pipe(notify({ message: 'Scripts task complete' }));
});

////
//
////
gulp.task('images', function() {

    return gulp.src('resources/images/**/*')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('assets/images'))
        .pipe(notify({ message: 'Images task complete' }));

});

////
// Watcher task for CSS/Script compilers
////
gulp.task('watch', function() {

    // Watch main styles scss file
    gulp.watch([
        'style.scss',
        'resources/frontend/theme/scss/*.scss'
    ], ['frontend-styles']);

    // Watch main styles scss file
    gulp.watch([
        'resources/admin/scss/admin.scss',
        'resources/admin/scss/parts/*.scss',
    ], ['admin-styles']);

    // Main libraries scss file
    gulp.watch([
        'resources/frontend/libraries/scss/libraries.scss',
        'node_modules/bootstrap/scss/*.scss',
        'node_modules/@fortawesome/fontawesome-free/scss/*.scss',
        'node_modules/slick-carousel/slick',
        'node_modules/slick-carousel/slick-theme'
    ], ['libraries']);

    // Scripts files
    gulp.watch([
        // worth splitting the libraries and the theme scripts in to separate files at some point
        'node_modules/slick-carousel/slick/slick.min.js',
        'resources/frontend/theme/js/scripts.js'
    ], ['scripts']);


    // compress images when added/removed
    gulp.watch('resources/images/**/*', ['images']);

});


////
// Clean before live
////
gulp.task('clean', function() {
    return del(['assets']);
});

////
//
////
gulp.task('default', ['clean'], function() {
    gulp.start('project-start', 'frontend-styles', 'admin-styles', 'libraries', 'scripts', 'images');
});