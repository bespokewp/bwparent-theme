( function( $ ) {

    $('document').ready(function() {

        $('body').on( 'click', '.menu-toggle', function( e ) {
            e.preventDefault();
            $('body').toggleClass('show-menu');
        });

        $('.slick-slider').on('setPosition', function () {
            resizeSlider();
        });

        //we need to maintain a set height when a resize event occurs.
        //Some events will through a resize trigger: $(window).trigger('resize');
        $(window).on('resize', function(e) {
            resizeSlider();

        });

        //since multiple events can trigger a slider adjustment, we will control that adjustment here
        function resizeSlider(){
            var $slickSlider = $('.slick-slider');
            $slickSlider.find('.slick-slide').height('auto');

            var slickTrack = $slickSlider.find('.slick-track');
            var slickTrackHeight = $(slickTrack).height();

            $slickSlider.find('.slick-slide').css('height', slickTrackHeight + 'px');
        }


        if ( $('.sticky-menu').length > 0 ) {

            amend_sticky_padding();
            trigger_on_scroll( $('.sticky-menu'), $('header'), 'end', 'past-marker', true );

            $(window).on('resize', function() {
                amend_sticky_padding();
            });


            $(window).on('scroll', function() {
                trigger_on_scroll( $('.sticky-menu'), $('header'), 'end', 'past-marker', true );
            });

        }

        function amend_sticky_padding() {

            var $sticky_menu = $('.sticky-menu');
            var sticky_height = $sticky_menu.outerHeight();

            $('body').css('padding-top', sticky_height);
        }

        /**
         *
         * Trigger JS when wind scrolls past element. Currently triggers when it hits the top of the screen
         * minus the elements height (overlaying elements)
         *
         * @param $target jQuery object of fixed element to trigger event on
         * @param $marker jQuery object that sets the tripwire
         * @param trigger string where to trigger the event (start/end of element in view)
         * @param custom_class string a custom class to add to the target element when event is triggered
         * @param fixed if target element is fixed, add true to trigger event
         */
        function trigger_on_scroll( $target, $marker, trigger, custom_class ) {

            var target_height = $target.outerHeight();

            var marker_height = $marker.outerHeight();
            var current_scroll_position = $(document).scrollTop();

            var bottom_of_header = $marker.offset().top - target_height;

            if ( trigger === 'end' )
                bottom_of_header += marker_height;

            // just in case the admin bar is showing..
            if ( $('#wpadminbar').length >= 1 )
                bottom_of_header -= $('#wpadminbar').outerHeight();

            if ( bottom_of_header <= current_scroll_position )
                $target.addClass(custom_class);
            else
                $target.removeClass(custom_class);
        }


        // Select all links with hashes
        $('a[href*="#"]')
        // Remove links that don't actually link to anything
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {

                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                    &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });

    });

})( jQuery );



