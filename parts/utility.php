<?php



/***
 * Utility functions
 ***/
	if ( !function_exists( 'fsi' ) ) :

		/**
		 * Forward Slash It (fsi)
		 * Add a trailing slash on to the end of a string
		 * @param string $string
		 *
		 * @return string;
		 */
		function fsi( $string ) {
			return  rtrim( $string, '/') . '/';
		}

	endif;

	if ( !function_exists('dump') ) :

		/**
		 * Echo out a variable within pre tags. Useful for arrays and objects
		 * @param $var mixed
		 * @param $die bool  if true, run the 'die' function and kill after dumping to screen
		 */
		function dump( $var, $die = false ) {
			echo '<pre>' . print_r( $var, true ) . '</pre>';
			if ( $die )
				die;
		}

	endif;

	if ( !function_exists('get_key_pos') ) :

		/**
		 * Get index of array item based on key
		 * @param $array array
		 *
		 * @return int|bool
		 */
		function get_key_pos( $key, $array ) {
			return array_search( $key, array_keys( $array ) );
		}

	endif;

	if ( !function_exists( 'add_item_after_pos' ) ) :

		/**
		 * Add item to array at specific point
		 * @param $item mixed           The data to add to the array
		 * @param $pos int              The numerical position of item to add the $item after
		 * @param $array array          The original array to add the items to
		 * @param $key string|bool      A key for the item, if needed
		 *
		 * @return array
		 */
		function add_item_after_pos( $item, $pos, $array, $key = false ) {

			$pos++;
			$part_1 = array_slice( $array, 0, $pos, true );


			if ( is_string( $key ) || is_int( $key ) )
				$part_1[$key] = $item;

			else
				$part_1[] = $item;

			$c = count( $array ) + 1;
			$part_2 = array_slice(
				$array,
				$pos,
				$c,
				true
			);

			return array_merge( $part_1, $part_2 );

		}

	endif;

	if ( !function_exists('cto') ) :

		/**
		 * Shortcut function for 'carbon_get_theme_option'
		 * @param $option_name string
		 * @return mixed
		 */
		function cto( $option_name ) {
			return carbon_get_theme_option( $option_name );
		}

	endif;

/***
 * Remove bloatware
 ***/
	/**
	 * Disable the emoji's
	 */
	function disable_emojis() {
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		remove_action( 'admin_print_styles', 'print_emoji_styles' );
		remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
		remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
		remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
		add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
		add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
	}
	add_action( 'init', 'disable_emojis' );

	/**
	 * Filter function used to remove the tinymce emoji plugin.
	 *
	 * @param array $plugins
	 * @return array Difference betwen the two arrays
	 */
	function disable_emojis_tinymce( $plugins ) {
		if ( is_array( $plugins ) ) {
			return array_diff( $plugins, array( 'wpemoji' ) );
		} else {
			return array();
		}
	}

	/**
	 * Remove emoji CDN hostname from DNS prefetching hints.
	 *
	 * @param array $urls URLs to print for resource hints.
	 * @param string $relation_type The relation type the URLs are printed for.
	 * @return array Difference betwen the two arrays.
	 */
	function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
		if ( 'dns-prefetch' == $relation_type ) {
			/** This filter is documented in wp-includes/formatting.php */
			$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

			$urls = array_diff( $urls, array( $emoji_svg_url ) );
		}

		return $urls;
	}