<?php



/**
 * Takes a string of text and replaces dynamic placeholders with relevant data
 *
 * @param $text
 *
 * @return mixed
 */

if ( !function_exists('bwp_process_dynamic_tags') ) :
	function bwp_process_dynamic_tags( $text ) {

		/**
		 * Allow child themes to add their own dynamic tags. The key of the array acts as the tag (no `{{  }}` required,
		 * just the tag name) and the corresponding value will replace the tag in the text
		 */
		$custom_dynamic_tags_before = apply_filters( 'bwp_custom_dynamic_tag_before_defaults', array() );
		dynamic_tag_find_replace( $custom_dynamic_tags_before, $text );

		/**
		 * Allow users to modify default tag args
		 */
		$defaults = apply_filters( 'bwp_modify_dynamic_tag_defaults', array(
			'year' => date('Y'),
			'month' => date('M'),
			'day' => date('d'),
			'company_name' => carbon_get_theme_option( 'bwp_company_name' )
		) );
		dynamic_tag_find_replace( $defaults, $text );



		/**
		 * @see `bwp_custom_dynamic_tag_before_defaults` filter above
		 */
		$custom_dynamic_tags_after = apply_filters( 'bwp_custom_dynamic_tag_after_defaults', array() );
		dynamic_tag_find_replace( $custom_dynamic_tags_after, $text );


		return $text;

	}
endif;

if ( !function_exists('dynamic_tag_find_replace') ) :
	function dynamic_tag_find_replace( $filters, &$text ) {

		// find/replace each tag with the value
		foreach ( $filters as $tag => $value)
			$text = str_replace( '{{ ' . $tag . ' }}', $value, $text );

	}
endif;

/**
 * Renders a social template part based on the slug parsed in
 *
 * @param string $slug  the slug of the template part
 */

if ( !function_exists('bwp_social_share_list') ) :
	function bwp_social_share_list( $slug = 'main' ) {
		get_template_part( 'templates/socials/social', $slug );
	}
endif;

/**
 * Generate header slider mark up from template
 */
if ( !function_exists('bwp_render_header_slider') ) :
	function bwp_render_header_slider() {
		if ( cto('bwp_header_slider_module') )
			get_template_part( 'templates/sliders/slider', 'header' );
	}
endif;


/**
 * Generate sticky menu mark up from template
 */
if ( !function_exists('bwp_render_sticky_menu') ) :
	function bwp_render_sticky_menu() {
        get_template_part( 'templates/menus/menu', 'sticky' );
	}
endif;


/**
 * Generate home link mark up from template
 */
if ( !function_exists('bwp_render_home_link') ) :
	function bwp_render_home_link() {
		get_template_part('templates/headers/header', 'home-link');
	}
endif;