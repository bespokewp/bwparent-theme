<?php $menu_position = cto('bwp_menu_style' );?>
<?php $item_alignment =  cto('bwp_item_alignment'); ?>

<div class="sticky-menu__outer-container sticky-menu">

    <div class="sticky-menu__inner-container container">

        <div class="row">

	        <?php // make room for the logo if needed ?>

	        <?php $logo_col = $item_alignment === 'inline' || is_null($item_alignment ) ? 3 : 12;?>
            <?php $menu_col =
                ( $menu_position === 'sticky-w-logo' ) ?
	                $item_alignment === 'inline'
	                || is_null( $item_alignment ) ?
                    9 // if logo is left aligned
                    : 12 // if logo is centered
                : 12; // if logo does not exist ?>

            <?php // if the logo needs to be included in the sticky nav ?>
            <?php if ( $menu_position === 'sticky-w-logo') : ?>
                <div class="col-<?php echo $logo_col; ?> sticky-menu__container--left">
                    <div class="sticky-menu__container<?php if ( $item_alignment === 'centered') : ?>--centered <?php endif ?> sticky-menu__container--logo">
                        <?php bwp_render_home_link(); ?>
                    </div>
                </div>
            <?php endif; ?>


	        <?php // create nav ?>
            <div class="col-<?php echo $menu_col; ?> sticky-menu__container<?php if ( $item_alignment === 'centered') : ?>--centered<?php else :?>--right<?php endif ?>">

                <div class="sticky-menu__container sticky-menu__container--menu">
	                <?php wp_nav_menu( array(
		                'menu' => 'main-menu',
		                'menu_class' => 'header__menu--sticky menu-list',
		                'container'  => 'nav',
		                'container_class' => 'header__menu-container',
                        'fallback_cb' => false
	                ) ); ?>
                </div>

                <?php if ( cto('bwp_display_social_icons') ) : ?>

                    <?php $social_accounts = carbon_get_theme_option('bwp_social_accounts'); ?>

                    <?php if ( !empty( $social_accounts ) ) : ?>
                        <div class="sticky-menu__container sticky-menu__container--social">
                            <?php get_template_part( 'templates/socials/social', 'main' ); ?>
                        </div>
                    <?php  endif; ?>

                <?php  endif; ?>

            </div>

        </div>

    </div>

</div>
