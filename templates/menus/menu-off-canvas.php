<div class="header__off-canvas-container--right off-canvas off-canvas-right">

	<a href="#" class="menu-toggle">close</a>

	<?php wp_nav_menu( array(
		'menu' => 'main-menu',
		'menu_class' => 'header__menu--off-canvas menu-list',
		'container'  => 'nav',
		'container_class' => 'header__menu-container--below'
	) ); ?>

</div>