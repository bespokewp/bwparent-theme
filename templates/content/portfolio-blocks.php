<?php

$portfolio_args = array(
	'post_type'  => 'bwp_portfolio',
	'posts_per_page' => -1,
);

$portfolio_posts = new WP_Query( $portfolio_args );

if ( $portfolio_posts->have_posts() ) :

    while ( $portfolio_posts->have_posts() ) :

        $portfolio_posts->the_post(); ?>

        <div class="portfolio-item__outer-container">

            <div class="portfolio-item__inner-container container">

                <div class="row">

                    <div class="col-12" itemscope itemtype="http://schema.org/CreativeWork">

                        <h4 itemprop="name"><?php the_title(); ?></h4>

                        <div itemprop="description">
                            <?php the_content(); ?>
                        </div>

                        <?php $portfolio_link = carbon_get_post_meta( $post->ID, 'portfolio_link'); ?>
                        <?php if ( $portfolio_link ) : ?>
                            <a class="btn btn-primary team-cta" href="<?php echo $portfolio_link; ?>" target="_blank">View project</a>
                        <?php endif; ?>

                    </div>

                </div>

            </div>

        </div>
    
    <?php endwhile;

    wp_reset_postdata();

endif;