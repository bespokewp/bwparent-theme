<div itemscope itemtype="http://schema.org/Organization">

	<?php if ( isset( $address_lines['bwp_company_name'] ) ) : ?>
        <p><span itemprop="legalName"><?php echo $address_lines['bwp_company_name']; ?></span></p>
	<?php endif; ?>

	<div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">

        <address>
	        <span itemprop="streetAddress">
		        <?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_building' ) ) )
		            echo carbon_get_theme_option('bwp_company_address_building' ) . ',<br>'; ?>

		        <?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_sub_building' ) ) )
		            echo carbon_get_theme_option('bwp_company_address_sub_building' ) . ',<br>'; ?>


		        <?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_business_park' ) ) )
		            echo carbon_get_theme_option('bwp_company_address_business_park' ) . ',<br>'; ?>

		        <?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_street' ) ) )
			        echo carbon_get_theme_option('bwp_company_address_street' ) . ',<br>'; ?>
            </span>
        </address>

		<p>
			<?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_locality' ) ) ) : ?>
				<span itemprop="addressLocality"><?php echo carbon_get_theme_option('bwp_company_address_locality' ); ?></span>,<br>
			<?php endif; ?>

			<?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_post_town' ) ) ) : ?>
				<span itemprop="addressRegion"><?php echo carbon_get_theme_option('bwp_company_address_post_town' ); ?></span>,<br>
			<?php endif; ?>

			<?php if ( !empty( carbon_get_theme_option( 'bwp_company_address_post_code' ) ) ) : ?>
				<span itemprop="postalCode"><?php echo carbon_get_theme_option('bwp_company_address_post_code' ); ?></span>
			<?php endif; ?>
		</p>

	</div>

	<?php if ( !empty( carbon_get_theme_option('bwp_company_main_number' ) ) ) :
		$tel = carbon_get_theme_option('bwp_company_main_number' ); ?>
		<p><a href="tel:<?php echo $tel; ?>"><span itemprop="telephone"><?php echo $tel; ?></span></a></p>
	<?php endif; ?>

	<?php if ( !empty( carbon_get_theme_option('bwp_company_main_email' ) ) ) :
		$email = carbon_get_theme_option('bwp_company_main_email' ); ?>
		<p><a href="mailto:<?php echo $email; ?>"><span itemprop="email"><?php echo $email; ?></span></a></p>
	<?php endif; ?>

</div>