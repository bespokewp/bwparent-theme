<?php $primary_colophon = carbon_get_theme_option( 'bwp_footer_colophon_text' ); ?>
<?php $secondary_colophon = carbon_get_theme_option( 'bwp_footer_secondary_colophon_text' ); ?>
<?php $social_colophon = carbon_get_theme_option( 'bwp_footer_colophon_social_links' ); ?>

<div class="container">

	<div class="row">

		<div class="col-12">

			<ul class="footer__colophon-list">
				<?php if ( !empty( $primary_colophon )) : ?>
					<li><p><?php echo bwp_process_dynamic_tags( $primary_colophon ); ?></p></li>
				<?php endif; ?>

				<?php if ( !empty( $secondary_colophon ) ) : ?>
					<li><p><?php echo $secondary_colophon; ?></p></li>
				<?php endif; ?>


				<?php if ( $social_colophon ) : ?>
					<li>
						<?php bwp_social_share_list(); ?>
                    </li>
				<?php endif; ?>

			</ul>


		</div>

	</div>

</div>
