<?php $social_accounts = carbon_get_theme_option('bwp_social_accounts'); ?>
<?php $include_email = carbon_get_theme_option('bwp_include_email_social'); ?>
<?php $include_phone = carbon_get_theme_option('bwp_include_contact_number'); ?>

<?php if (
        !empty( $social_accounts )
        || !$include_email
        || !$include_phone
    ) : ?>

	<ul class="social-icons" itemscope itemtype="http://schema.org/Organization">

        <li class="sr-only">
            <link itemprop="url" href="<?php site_url(); ?>"></li>


        <li class="sr-only">
            <link itemprop="url" href="<?php echo site_url(); ?>">
        </li>

		<?php foreach ( $social_accounts as $social_account ) :

            if ( $social_account['bwp_social_include_follow'] ) :

                $icon = false;

	            switch ( $social_account['bwp_social_type'] ) :
		            case 'Facebook':
			            $icon = '<i class="fab fa-facebook-f"></i>';
			            break;
		            case 'Twitter':
			            $icon = '<i class="fab fa-twitter"></i>';
			            break;
		            case 'Google+':
			            $icon = '<i class="fab fa-google-plus-g"></i>';
			            break;
		            case 'LinkedIn':
			            $icon = '<i class="fab fa-linkedin-in"></i>';
			            break;
		            case 'Instagram':
			            $icon = '<i class="fab fa-instagram"></i>';
			            break;
		            case 'Bitbucket':
			            $icon = '<i class="fab fa-bitbucket"></i>';
			            break;

	            endswitch;

	            if ( $icon ) : ?>

                    <li><a  itemprop="sameAs" href="<?php echo $social_account['bwp_social_url']; ?>" target="_blank"><?php echo $icon; ?></a></li> <?php

	            endif;

			endif;

		 endforeach; ?>

        <?php if ( $include_email ) : ?>
            <?php $email = cto('bwp_email_social'); ?>

            <li><a href="mailto:<?php echo $email; ?>">Email: <span itemprop="email"><?php echo $email?></span></a></li>
        <?php endif; ?>

        <?php if ( $include_phone ) : ?>
	        <?php $phone = cto('bwp_phone_social'); ?>
            <li><a href="tel:<?php echo $phone; ?>">Phone:  <span itemprop="telephone"><?php echo $phone; ?></span></a></li>
        <?php endif; ?>

	</ul>
<?php  endif; ?>