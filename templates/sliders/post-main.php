<?php global $carousel_id, $slide_details; ?>

<?php if ( isset( $slide_details[0]['bwp_slider_image'] ) && !empty($slide_details[0]['bwp_slider_image'] ) ) : ?>
    <div class="container">

        <div class="row">

            <div class="col-12">

                <ul id="slider-<?php echo $carousel_id; ?>" class="carousel">

                    <?php
                    /** @var WP_Post $carousel_post */
                    foreach ( $slide_details[0]['bwp_post_slides'] as $carousel_post ) : ?>
                        <li>
                            <?php echo $carousel_post->post_title; ?>
                        </li>
                    <?php endforeach; ?>

                </ul>

            </div>

        </div>

    </div>
<?php endif; ?>