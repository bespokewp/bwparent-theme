<?php global $carousel_id, $slide_details; ?>

<?php if ( isset( $slide_details[0]['bwp_slider_image'] ) && !empty($slide_details[0]['bwp_slider_image'] ) ) : ?>
    <div class="container">

        <div class="row">

            <div class="col-12">

                <ul id="slider-<?php echo $carousel_id; ?>" class="carousel">

                    <?php foreach ( $slide_details[0]['bwp_slider_image'] as $carousel_image ) : ?>
                        <li><img src="<?php echo wp_get_attachment_image_url( $carousel_image, 'thumbnail' )?>"></li>
                    <?php endforeach; ?>

                </ul>

            </div>

        </div>

    </div>
<?php endif; ?>