<?php global $carousel_id, $slide_details; ?>

<div class="container">

	<div class="row">

		<div class="col-12">

			<ul id="slider-<?php echo $carousel_id; ?>" class="carousel">

				<?php foreach ( $slide_details[0]['bwp_testimonials'] as $testimonial ) : ?>
					<li>
                        <div>
                            <?php echo $testimonial['bwp_testimonial_review']; ?>
                        </div>
                        <h3><?php echo $testimonial['bwp_testimonial_name']; ?></h3>
                        <p><?php echo $testimonial['bwp_testimonial_date']; ?></p>
                    </li>
				<?php endforeach; ?>

			</ul>

		</div>

	</div>

</div>
