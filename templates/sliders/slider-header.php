<?php global $post; ?>

<?php if ( isset( $post->ID ) ) : ?>


    <?php $header_slides = carbon_get_post_meta($post->ID, 'bwp_header_slides'); ?>
    <?php if ( !empty($header_slides) ) : ?>

        <div class="header__slider">

            <div id="slider-<?php echo $post->ID; ?>" class="carousel">

                <?php foreach ( $header_slides as $key => $header_slide ) : ?>

                    <div class="slider-<? echo $key; ?> slider__single-slide">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">

                                    <?php if ( !empty( $header_slide['bwp_header_slides_title'] ) ) : ?>
                                        <h2><?php echo $header_slide['bwp_header_slides_title']; ?></h2>
                                    <?php endif; ?>

                                    <?php if ( !empty( $header_slide['bwp_header_slides_content'] ) ) : ?>
                                        <p><?php echo $header_slide['bwp_header_slides_content']; ?></p>
                                    <?php endif; ?>

                                    <?php $ctas = $header_slide['bwp_header_ctas']; ?>

                                    <?php if ( !empty( $ctas ) ) : ?>

                                        <p>

                                            <?php foreach ( $ctas as $key_i => $cta ) :?>

                                                <?php if (
                                                        !empty($cta['bwp_header_slides_button_link'] )
                                                        && !empty( $cta['bwp_header_slides_button_text'] )
                                                ) : ?>

                                                    <?php $target = $cta['bwp_header_cta_target']; ?>

                                                    <?php switch ( $target ) :
                                                        case 'new-tab' :
                                                            $target_html = ' target="_blank"';
                                                            break;

                                                        default :
                                                            $target_html = '';
                                                            break;

                                                        endswitch; ?>

                                                    <a class="btn btn-primary header-cta-<?php echo $key_i; ?>" href="<?php echo $cta['bwp_header_slides_button_link']; ?>"<?php echo $target_html; ?>><?php echo $cta['bwp_header_slides_button_text']; ?></a>
                                                <?php endif; ?>

                                            <?php endforeach ;?>
                                        </p>

                                    <?php endif;?>

                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>

        </div>

    <?php endif; ?>

<?php endif; ?>
