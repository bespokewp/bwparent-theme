<?php $menu_position = cto( 'bwp_menu_style' ); ?>
<?php $logo = cto( 'bwp_header_logo' ); ?>

<header>

	<?php if ( $menu_position === 'sticky' || $menu_position === 'sticky-w-logo' ) :
	    bwp_render_sticky_menu(); ?>
	 <?php


	else : ?>

        <div class="header__outer-container">
            <div class="header__inner-container container">
                <div class="row">

	                <?php $item_alignment =  cto('bwp_item_alignment'); ?>
	                <?php $logo_col = $item_alignment === 'inline' || is_null( $item_alignment ) ? 3 : 12;?>
	                <?php $menu_col = $item_alignment === 'inline' || is_null( $item_alignment ) ? 9 : 12;?>

                    <div class="header__logo-container<?php if ( $item_alignment === 'centered' ) : ?>--centered<?php endif;?> col-md-<?php echo $logo_col; ?>">

                        <?php if ( $menu_position !== 'sticky-w-logo')
                            bwp_render_home_link(); ?>
                    </div>


                    <div class="header__menu-container col-md-<?php echo $menu_col; ?>">
                        <div class="row header-menu__inner-container">

                            <?php if (  $menu_position === 'off-canvas' ) : ?>
                                <div class="col-12 text-right">
                                    <div style="display: table; height: 100%; width: 100%;">
                                        <div style="display: table-cell; vertical-align: middle">
                                            <a class="menu-toggle" href="#">Menu</a>
                                        </div>
                                    </div>
                                </div>
                            <?php  endif; ?>

                            <?php // Menu to the right of the logo ?>
                            <?php if ( $menu_position === 'header'|| $menu_position === null  ) : ?>

                                <div class="col-12 header__container<?php if ( $item_alignment === 'centered' ) : ?>--centered<?php else : ?>--right<?php endif; ?>">

                                    <?php wp_nav_menu( array(
                                        'menu' => 'main-menu',
                                        'menu_class' => 'header__menu menu-list',
                                        'container'  => 'nav',
                                        'container_class' => 'header__menu-container'
                                    ) ); ?>

	                                <?php if (
		                                cto('bwp_display_social_icons')
		                                && $menu_position !== 'sticky'
		                                && $menu_position !== 'sticky-w-logo'
	                                ) : ?>

		                                <?php $social_accounts = carbon_get_theme_option('bwp_social_accounts'); ?>

		                                <?php if ( !empty( $social_accounts ) ) : ?>
                                            <div class="header__social-container">
				                                <?php get_template_part( 'templates/socials/social', 'main' ); ?>
                                            </div>
		                                <?php  endif; ?>

	                                <?php  endif; ?>
                                </div>

                            <?php  endif; ?>

                        </div>
                    </div>

                    <?php // Menu below header ?>
                    <?php if ( $menu_position === 'below-header' ) : ?>
                        <div class="col-12<?php if ( $item_alignment === 'centered') : ?> text-center<?php endif; ?> header__container<?php if ( $item_alignment === 'centered' ) : ?>--centered<?php else : ?>--right<?php endif; ?>">
                            <?php wp_nav_menu( array(
                                'menu' => 'main-menu',
                                'menu_class' => 'header__menu menu-list',
                                'container'  => 'nav',
                                'container_class' => 'header__menu-container--below'
                            ) ); ?>
	                        <?php if (
		                        cto('bwp_display_social_icons')
		                        && $menu_position !== 'sticky'
		                        && $menu_position !== 'sticky-w-logo'
	                        ) : ?>

		                        <?php $social_accounts = carbon_get_theme_option('bwp_social_accounts'); ?>

		                        <?php if ( !empty( $social_accounts ) ) : ?>
                                    <div class="header__social-container">
				                        <?php get_template_part( 'templates/socials/social', 'main' ); ?>
                                    </div>
		                        <?php  endif; ?>

	                        <?php  endif; ?>
                        </div>
                    <?php  endif; ?>

                </div>

                <?php // Off Canvas menu ?>
                <?php  if ( $menu_position === 'off-canvas' ) : ?>
                    <?php get_template_part( 'templates/menus/menu', 'off-canvas' ); ?>
                <?php  endif; ?>

            </div>
        </div>

    <?php endif; ?>

	<?php bwp_render_header_slider(); ?>

</header>