<?php // template for the link holding the site logo or site link in the header ?>

<?php $site_name = get_bloginfo( 'name' ); ?>
<?php $logo = cto( 'bwp_header_logo' ); ?>
<?php $logo_link = is_front_page() ? '#top' : get_site_url(); ?>

<a href="<?php echo $logo_link; ?>">
	<?php if ( $logo ) : ?>

		<img class="header__logo" srcset="<?php echo wp_get_attachment_image_srcset( $logo, 'small' ); ?>" alt="company logo">

		<?php if ( is_front_page() ) : ?>
			<div class="sr-only" itemscope itemtype="http://schema.org/Organization">
				<h1><span itemprop="alternateName"><?php echo $site_name; ?></span></h1>
			</div>
		<?php else : ?>
			<p class="sr-only"><?php echo $site_name; ?></p>
		<?php endif; ?>

	<?php else : ?>

		<?php if ( is_front_page() ) : ?>
			<div itemscope itemtype="http://schema.org/Organization">
				<h1><span itemprop="alternateName"><?php echo $site_name; ?></span></h1>
			</div>
		<?php else : ?>
			<p class="h1"><?php echo $site_name; ?></p>
		<?php endif; ?>

	<?php endif; ?>
</a>