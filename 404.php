<?php get_header(); ?>

	<main>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1>404</h1>
					<p>Sorry. That page does not exist.</p>
				</div>
			</div>
		</div>
	</main>

<?php get_footer(); ?>