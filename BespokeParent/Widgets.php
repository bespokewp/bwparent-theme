<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 28/09/2018
 * Time: 09:18
 */

namespace BespokeParent;


class Widgets {

	public function __construct() {
		add_action( 'widgets_init', array( $this, 'theme_slug_widgets_init' ) );
	}

	public function theme_slug_widgets_init() {

		$footer_widget_count = ( carbon_get_theme_option('bwp_footer_widgets_total') ) ? carbon_get_theme_option('bwp_footer_widgets_total') : 0;
		$i = 1;

		while ( $i <= $footer_widget_count ) :

			register_sidebar( array(
				'name' => 'Footer area ' . $i,
				'id' => 'footer-area-' . $i,
				'description' => __( 'Widgets in this area will display in column number ' . $i . ' in the footer'),
				'before_widget' => '<li id="%1$s" class="widget %2$s">',
				'after_widget'  => '</li>',
				'before_title'  => '<h2 class="widgettitle">',
				'after_title'   => '</h2>',
			) );

			$i++;

		endwhile;

	}


}