<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 16/09/2018
 * Time: 11:24
 */

namespace BespokeParent\Features;

 class Slider {

	protected
		$slider_id,
		$suffix,
		$responsive_suffix;

	 /**
	  * Slider constructor.
	  *
      * Takes a post ID and renders slick slider options based on it's options
      * Note: slider options
      * Note: slider options
      *
	  * @param $slider_id
	  */
	public function __construct( $slider_id ) {

		$this->setSliderId( $slider_id );

		add_action( 'wp_footer', array( $this, 'renderScript' ), 100 );

	}

	/**
	 * @return int
	 */
	protected function getSliderId() {
		return $this->slider_id;
	}

	/**
	 * @param int $slider_id
	 */
	private function setSliderId( $slider_id ) {
		$this->slider_id = $slider_id;
	}

    /**
     * @return array
     */
    protected function getScriptOptions() {

		$slider_id = $this->getSliderId();

		$carousel_speed = carbon_get_post_meta( $slider_id, 'bwp_speed' );
		$slides_to_show = carbon_get_post_meta( $slider_id, 'bwp_slides_to_show' );
		$arrows = carbon_get_post_meta( $slider_id, 'bwp_arrows' );
		$auto_speed =
			!is_numeric( $carousel_speed )
			|| empty( $carousel_speed )
			|| $carousel_speed <= 0
				? 0 : $carousel_speed;
		$dots = carbon_get_post_meta( $slider_id, 'bwp_dots' );

		$slides_to_show =
			!is_numeric( $slides_to_show )
			|| empty( $slides_to_show )
			|| $slides_to_show <= 0
				? 1 : $slides_to_show;

		$slider_options = array(
			'infinite'      => true,
			'slidesToShow'  => (int)$slides_to_show,
			'slidesToScroll'=> (int)$slides_to_show,
			'dots'          => $dots,
			'arrows'        => $arrows,
			'autoplay'      => $auto_speed > 0 ? true : false,
			'autoplaySpeed' => $auto_speed
		);

		$responsive_options = carbon_get_post_meta( $slider_id, 'bwp_media_options' );


		if ( !empty( $responsive_options ) ) :
			$slider_options['responsive'] = array();

			foreach ( $responsive_options as $options ) :

				$carousel_speed = isset( $options['bwp_speed_responsive'] ) ? $options['bwp_speed_responsive'] : 0;
				$slides_to_show = isset( $options['bwp_slides_to_show_responsive'] ) ? $options['bwp_slides_to_show_responsive'] : 1;

				$arrows = isset( $options['bwp_arrows_responsive'] ) ? $options['bwp_arrows_responsive'] : false;

				$auto_speed =
					!is_numeric( $carousel_speed )
					|| empty( $carousel_speed )
					|| $carousel_speed <= 0
						? 0 : $carousel_speed;

				$dots = isset( $options['bwp_dots_responsive'] ) ? $options['bwp_dots_responsive'] : false;

				$slides_to_show =
					!is_numeric( $slides_to_show )
					|| empty( $slides_to_show )
					|| $slides_to_show <= 0
						? 1 : $slides_to_show;

				$slider_options['responsive'][] = array(
					'breakpoint' => $options['bwp_slick_media_size'],
					'settings' => array(
						'infinite'      => true,
						'slidesToShow'  => (int)$slides_to_show,
						'slidesToScroll'=> (int)$slides_to_show,
						'dots'          => (bool)$dots,
						'arrows'        => (bool)$arrows,
						'autoplay'      => $auto_speed > 0 ? true : false,
						'autoplaySpeed' => (int)$auto_speed
					)

				);


			endforeach;

		endif;

		return $slider_options;

	}


	/**
	 * Render the scripts to run the slick slider for Carousels
	 */
	public function renderScript() {

		$gallery_id = $this->getSliderId();
		$slider_options = $this->getScriptOptions();

		ob_start() ?>

		<script>

            var defaultOptions = <?php echo json_encode($slider_options); ?>;

            jQuery('#slider-<?php echo $gallery_id; ?>').slick(defaultOptions);

		</script>

		<?php echo ob_get_clean();

	}


}