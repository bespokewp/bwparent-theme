<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 18/09/2018
 * Time: 08:40
 */

namespace BespokeParent\Features;

class HeaderSlider extends Slider {

	/**
	 * Render the scripts to run the slick slider for Carousels
	 */
	public function renderScript() {

	    if ( cto('bwp_header_slider_module') ) :

            $gallery_id = $this->getSliderId();


		    $header_slides = carbon_get_post_meta( $gallery_id, 'bwp_header_slides');


		    if ( count( $header_slides ) > 1 ) :

                $slider_options = $this->getScriptOptions(); ?>

                <script>
                    var defaultOptions = <?php echo json_encode($slider_options); ?>;
                    jQuery('#slider-<?php echo $gallery_id; ?>').slick( defaultOptions );
                </script> <?php

            endif;

        endif;

	}

}