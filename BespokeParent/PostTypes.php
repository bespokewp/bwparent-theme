<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 15/09/2018
 * Time: 12:33
 */

namespace BespokeParent;

class PostTypes {

	public function __construct() {

		add_action( 'init',  array( $this, 'bwp_portfolio' ), 0 );
		add_action( 'init', array( $this, 'bwp_carousels' ), 0 );
		add_action( 'init', array( $this, 'custom_teams' ), 0 );

		// modify 'post' to 'news'
		add_action( 'admin_menu', array( $this, 'change_post_label' ) );
		add_action( 'init', array( $this, 'change_post_object' ) );


		// remove the post types if they are unchecked in the admin options
		add_action( 'carbon_fields_fields_registered', function() {

			if ( !cto('bwp_teams_module') )
				unregister_post_type('bwp_teams');

			if ( !cto('bwp_carousel_module') )
				unregister_post_type('bwp_carousel');

			if ( !cto('bwp_portfolio_module') )
				unregister_post_type('bwp_portfolio');

		} );


	}

	public function bwp_portfolio() {

		$labels = array(
			'name'                  => _x( 'Portfolio', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Portfolio', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Portfolio', 'text_domain' ),
			'name_admin_bar'        => __( 'Portfolio', 'text_domain' ),
			'archives'              => __( 'Portfolio Archives', 'text_domain' ),
			'attributes'            => __( 'Portfolio Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Portfolio:', 'text_domain' ),
			'all_items'             => __( 'All Portfolio', 'text_domain' ),
			'add_new_item'          => __( 'Add New Portfolio', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Portfolio', 'text_domain' ),
			'edit_item'             => __( 'Edit Portfolio', 'text_domain' ),
			'update_item'           => __( 'Update Portfolio', 'text_domain' ),
			'view_item'             => __( 'View Portfolio', 'text_domain' ),
			'view_items'            => __( 'View Portfolio', 'text_domain' ),
			'search_items'          => __( 'Search Portfolio', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into Portfolio', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Portfolio', 'text_domain' ),
			'items_list'            => __( 'Portfolio list', 'text_domain' ),
			'items_list_navigation' => __( 'Portfolio list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter Portfolio list', 'text_domain' ),
		);
		$rewrite = array(
			'slug'                  => 'portfolio',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Portfolio', 'text_domain' ),
			'description'           => __( 'A collection of pieces', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies'            => array( 'category' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 15,
			'menu_icon'             => 'dashicons-slides',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => false,
		);
		register_post_type( 'bwp_portfolio', $args );

	}

	// Register Custom Slider Post Type
	public function bwp_carousels() {

		$labels = array(
			'name'                  => _x( 'Carousels', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Carousel', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Carousels', 'text_domain' ),
			'name_admin_bar'        => __( 'Carousels', 'text_domain' ),
			'archives'              => __( 'Carousel Archives', 'text_domain' ),
			'attributes'            => __( 'Carousel Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Carousel:', 'text_domain' ),
			'all_items'             => __( 'All Carousels', 'text_domain' ),
			'add_new_item'          => __( 'Add New Carousel', 'text_domain' ),
			'add_new'               => __( 'Add Carousel', 'text_domain' ),
			'new_item'              => __( 'New Carousel', 'text_domain' ),
			'edit_item'             => __( 'Edit Carousel', 'text_domain' ),
			'update_item'           => __( 'Update Carousel', 'text_domain' ),
			'view_item'             => __( 'View Carousel', 'text_domain' ),
			'view_items'            => __( 'View Carousels', 'text_domain' ),
			'search_items'          => __( 'Search Carousel', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into Carousel', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Carousel', 'text_domain' ),
			'items_list'            => __( 'Carousels list', 'text_domain' ),
			'items_list_navigation' => __( 'Carousels list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter Carousels list', 'text_domain' ),
		);
		$args   = array(
			'label'               => __( 'Carousel', 'text_domain' ),
			'description'         => __( 'Create custom sliders to display in the site', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 15,
			'menu_icon'           => 'dashicons-star-half',
			'show_in_admin_bar'   => true,
			'show_in_nav_menus'   => true,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'rewrite'             => false,
			'capability_type'     => 'page',
			'show_in_rest'        => false,
		);

		register_post_type( 'bwp_carousel', $args );

	}

	// Register Teams Post Type
	function custom_teams() {

		$labels = array(
			'name'                  => _x( 'Teams', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Teams', 'text_domain' ),
			'name_admin_bar'        => __( 'Team', 'text_domain' ),
			'archives'              => __( 'Team Archives', 'text_domain' ),
			'attributes'            => __( 'Team Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Team:', 'text_domain' ),
			'all_items'             => __( 'All Teams', 'text_domain' ),
			'add_new_item'          => __( 'Add New Team', 'text_domain' ),
			'add_new'               => __( 'Add Team', 'text_domain' ),
			'new_item'              => __( 'New Team', 'text_domain' ),
			'edit_item'             => __( 'Edit Team', 'text_domain' ),
			'update_item'           => __( 'Update Team', 'text_domain' ),
			'view_item'             => __( 'View Team', 'text_domain' ),
			'view_items'            => __( 'View Teams', 'text_domain' ),
			'search_items'          => __( 'Search Team', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into Team', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Team', 'text_domain' ),
			'items_list'            => __( 'Teams list', 'text_domain' ),
			'items_list_navigation' => __( 'Teams list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter Teams list', 'text_domain' ),
		);
		$rewrite = array(
			'slug'                  => 'teams',
			'with_front'            => false,
			'pages'                 => true,
			'feeds'                 => true,
		);
		$args = array(
			'label'                 => __( 'Team', 'text_domain' ),
			'description'           => __( 'Set up company teams here', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-groups',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'rewrite'               => $rewrite,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);

		register_post_type( 'bwp_teams', $args );

	}


	public function change_post_label() {
		global $menu;
		global $submenu;
		$menu[5][0]                 = 'News';
		$submenu['edit.php'][5][0]  = 'News';
		$submenu['edit.php'][10][0] = 'Add News';
		$submenu['edit.php'][16][0] = 'News Tags';
	}

	public function change_post_object() {
		/** @var \WP_Post_Type[] */
		global $wp_post_types;


		/** @var \WP_Post_Type */
		$posts                      = &$wp_post_types['post'];
		$posts->menu_icon           = 'dashicons-welcome-write-blog';
		$labels                     = $posts->labels;
		$labels->name               = 'News';
		$labels->singular_name      = 'News';
		$labels->add_new            = 'Add News';
		$labels->add_new_item       = 'Add News';
		$labels->edit_item          = 'Edit News';
		$labels->new_item           = 'News';
		$labels->view_item          = 'View News';
		$labels->search_items       = 'Search News';
		$labels->not_found          = 'No News found';
		$labels->not_found_in_trash = 'No News found in Trash';
		$labels->all_items          = 'All News';
		$labels->menu_name          = 'News';
		$labels->name_admin_bar     = 'News';
	}
}