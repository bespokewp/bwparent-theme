<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 15/09/2018
 * Time: 15:52
 */

namespace BespokeParent;


class ThemeSetup {

	public function __construct() {
		add_theme_support( 'post-thumbnails' );

		add_action( 'after_setup_theme', function() {
			register_nav_menu( 'main-menu', 'Main menu' );
			register_nav_menu( 'off-canvas-menu', 'Off canvas' );
		});
	}

}