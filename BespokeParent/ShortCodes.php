<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 15/09/2018
 * Time: 15:37
 */

namespace BespokeParent;

use BespokeParent\Features\Slider;


class ShortCodes {

	public function __construct() {


		// shortcode with fallback

        if ( cto('bwp_carousel_module') ) :

            if ( !shortcode_exists( 'carousel' ) ) :
                add_shortcode( 'carousel', array( $this, 'carousel' ) );
            elseif ( !shortcode_exists( 'bwp_carousel' ) ) :
                add_shortcode( 'bwp_carousel', array( $this, 'carousel' ) );
            endif;

		endif;

	}

	/**
	 * @param $attributes array
	 *
	 * @return string
	 */
	function carousel( $attributes ) {

		global $carousel_id, $slide_details;

		$attributes = shortcode_atts( array(
			'id' => false,
		), $attributes );

		// globals to parse to the template
		$carousel_id = $attributes['id'];

		// create the relevant carousel js in the header
		new Slider( $carousel_id );

		$slide_details = carbon_get_post_meta( $carousel_id, 'bwp_slider' );

		ob_start();

		if ( isset( $slide_details[0] ) ) :


			switch ( $slide_details[0]['_type'] ) :

				case 'testimonial_slider' :
					get_template_part( 'templates/sliders/testimonial', 'main' );
					break;

				case 'image_slider' :
					get_template_part( 'templates/sliders/carousel', 'main' );
					break;

				case 'post_slider' :
					get_template_part( 'templates/sliders/post', 'main' );
					break;

			endswitch;


		else : ?>

			<p>Carousel ID "<?php echo $carousel_id; ?>" does not exist. Please make sure your carousel ID is correct</p>

		<?php endif;



		return ob_get_clean();

	}


	/**
	 * @param $attributes array
	 *
	 * @return string
	 */
	function testimonials( $attributes ) {

		// to pass to the template
		global $testimonials_id;

		$attributes = shortcode_atts( array(
			'id' => false,
		), $attributes );

		// globals to parse to the template
		$testimonials_id = $attributes['id'];

		// create the relevant carousel js in the header
		new Slider( $testimonials_id );

		ob_start();

		get_template_part('templates/sliders/testimonial', 'main');

		return ob_get_clean();

	}

}