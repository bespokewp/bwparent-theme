<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 11/01/2019
 * Time: 14:22
 */

namespace BespokeParent\Tools;


class QuickCreate
{



	// top level
		// pages
		// posts
		// categories
		// tags

	// allow for nested items

	// Pages

	/**
	 * QuickCreate constructor.
	 * @param $parameters array 	Options passed in from the form to be used in the post creation
	 */
	public function __construct( $parameters )
	{

	}

	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return 'Test title';
	}

	/**
	 * @return string
	 */
	public function getContent(): string
	{
		return 'Test content';
	}

	/**
	 * @return string
	 */
	public function getExcerpt(): string
	{
		return 'Test excerpt';
	}

	/**
	 * @return string
	 */
	public function getStatus(): string
	{
		return 'draft';
	}


	/**
	 * @return string
	 */
	public function getPostType(): string
	{
		return 'post';
	}



	/**
	 * @return string
	 */
	public function getUserID(): string
	{
		return get_current_user_id();
	}




	function createPost()
	{

		$user_id = $this->getUserID();

		$post_data = [
			'post_title' => $this->getTitle(),
			'post_content' => $this->getContent(),
			'post_author' => $user_id,
			'post_excerpt' => $this->getExcerpt(),
			'post_status' => $this->getStatus(),
			'post_type' => $this->getPostType(),
		];


		// ID|WP_ERROR on success/fail
		wp_insert_post( $post_data );

	}


}