<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 15/09/2018
 * Time: 12:51
 */

namespace BespokeParent\Options;


use Carbon_Fields\Container;
use Carbon_Fields\Field as Field;

class ThemeOptions {

	private $defaultFields;

	public function __construct() {

		$this->defaultFields = array(
			'bwp_header_logo' =>
				Field::make('image', 'bwp_header_logo', 'Site logo'),
			'bwp_show_search_icon' =>
				Field::make( 'checkbox', 'bwp_show_search_icon', 'Show search icon?' )
				     ->help_text( 'Append a search icon to the menu bar' )
				     ->set_width( 50 ),
			'bwp_show_login_icon' =>
				Field::make( 'checkbox', 'bwp_login_icon', 'Show login icon?' )
				     ->help_text( 'Append a login icon to the menu bar' )
				     ->set_width( 50 ),
		);
		add_action( 'carbon_fields_register_fields', array( $this, 'crb_attach_theme_options' ) );
	}

	/**
	 * Carbon Fields theme options container creation
	 */
	public function crb_attach_theme_options() {

		$default_cf = $this;

		$container = Container::make( 'theme_options', __( 'Theme options', 'crb' ) )
		                      ->set_icon('dashicons-schedule');


		// General options
		$container
			->add_tab( 'General', $default_cf->getGeneralOptions() )
			->add_tab( 'Header', $default_cf->getHeaderLayoutOptions() )
			->add_tab( 'Footer', $default_cf->getFooterOptions() )
			->add_tab( 'Social', $default_cf->getSocialOptions() );

		// sub page options
		Container::make( 'theme_options', __( 'Modules', 'crb' ) )
		                          ->set_page_parent( $container )
		                          ->add_fields( $default_cf->getModuleOptions() );



	}
	/**
	 * @return array
	 */
	public function getDefaultHeaderOptions() {
		return $this->defaultFields;
	}

	/**
	 * @param array $defaultFields
	 */
	public function setDefaultFields( array $defaultFields ) {
		$this->defaultFields = $defaultFields;
	}

	/**
	 * @return array
	 */
	public function getGeneralOptions() {

		$company_options = array(
			Field::make('separator','company_contact_details', 'Company contact detail' ),
			Field::make('text', 'bwp_company_main_number', 'Main phone number')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_main_email', 'Main email address')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless theme-options__extra-margin'),
			Field::make('separator', 'company_address', 'Company address')
			     ->set_help_text('All fields are optional'),
			Field::make('text', 'bwp_company_name', 'Company name')
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_building', 'Building/Premise')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_sub_building', 'Sub-building')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_business_park', 'Business park')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_street', 'Name of thoroughfare (street)')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_locality', 'Dependent Locality (large villages)')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_post_town', 'Post Town')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_county', 'County')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless'),
			Field::make('text', 'bwp_company_address_post_code', 'Postcode')
			     ->set_width(50)
			     ->set_classes('theme-options__borderless theme-options__extra-margin'),

		);


		return $company_options;

	}

	/**
	 * @return array
	 */
	public function getSocialOptions() {

		return array(

			Field::make('complex', 'bwp_social_accounts', 'Social')
			     ->add_fields( array(

				     Field::make('select', 'bwp_social_type', 'Choose an account type')
				          ->add_options(
					          array(
						          'Facebook' => 'Facebook',
						          'Twitter' => 'Twitter',
						          'Google+' => 'Google+',
						          'LinkedIn' => 'LinkedIn',
						          'Instagram' => 'Instagram',
						          'Bitbucket' => 'Bitbucket'
					          )
				          ),

				     Field::make( 'text', 'bwp_social_url', 'Profile link' ),

				     Field::make( 'checkbox', 'bwp_social_include_follow', 'Include in follow links' )
				          ->set_width( 50 )
				          ->set_help_text( 'Add links that do not cahre content but send users to your social profile' ),

				     Field::make( 'checkbox', 'bwp_social_include_share', 'Include in share links' ) // todo: use http://sharelinkgenerator.com/ to generate basic markup for theme's share module
				          ->set_width( 50 )
				          ->set_help_text( 'Links appended to content areas to share the pages on the users social media' ),

			     ) )
			     ->set_layout( 'tabbed-horizontal' )
			     ->set_header_template( '
					<% if ( bwp_social_type ) { %>
					        <%- bwp_social_type %>
					    <% }  else { %>
					        Social feed
					    <% } %>
					' ),

			Field::make('checkbox', 'bwp_include_email_social', 'Email in follow links')
				->set_help_text('Tick this box to include an email address in the social links')
				->set_width(25),

			Field::make('text', 'bwp_email_social', 'Social email address')
				->set_help_text('The address that will be used in the social fields')
				->set_width(75),

			Field::make('checkbox', 'bwp_include_contact_number', 'Phone number in follow links')
				->set_help_text('Tick this box to include a phone number in the social links')
				->set_width(25),

			Field::make('text', 'bwp_phone_social', 'Social phone number')
				->set_help_text('The phone number that will be used in the social fields')
				->set_width(75),

		);

	}

	/**
	 * An array of Carbon fields that populate in the 'Modules' tab in the theme options
	 *
	 * @filter bwp_module_options
	 * @return Field[]
	 */
	public function getModuleOptions() {

		return apply_filters( 'bwp_module_options', array(
			Field::make( 'separator', 'bwp_modules', 'Available modules' )
			     ->set_help_text('Choose which modules to activate on the site'),
			Field::make('checkbox', 'bwp_header_slider_module', 'Header slider')
			     ->set_classes('theme-options__borderless')
			     ->set_help_text('add options for header sliders to be added to pages'),
			Field::make('checkbox', 'bwp_carousel_module', 'Carousels')
			     ->set_classes('theme-options__borderless')
			     ->set_help_text('add a \'Carousel\' post type to the site for testimonials, galleries or posts'),
			Field::make('checkbox', 'bwp_teams_module', 'Teams')
			     ->set_classes('theme-options__borderless')
			     ->set_help_text('add a \'Teams\' post type and add employees to them'),
			Field::make('checkbox', 'bwp_social_module', 'Social')
			     ->set_classes('theme-options__borderless')
			     ->set_help_text('add social links to the site'),
			Field::make('checkbox', 'bwp_portfolio_module', 'Portfolio')
			     ->set_classes('theme-options__borderless')
			     ->set_help_text('add portfolio pages to the site'),

		) );

	}


	public function getFooterOptions() {

		$options = array();


			$options[] = Field::make( 'text', 'bwp_footer_widgets_total', 'Number of widget areas' )
		                  ->set_attributes( array(
			                  'type' => 'number',
			                  'min'   => 0,
			                  'max'   => 4
		                  ) )
		                  ->set_help_text('choose how many widgets need to appear in the footer (min 0, max 8)')
		                  ->set_width( 100 );

		$options = array_merge( $options, array(

			Field::make('separator','bwp_colophon', 'Colophon' ),

			Field::make( 'text', 'bwp_footer_colophon_text', 'Colophon text' )
			     ->set_help_text('Use the following dynamic tags to customise the text area: <code>{{ year }}</code>, <code>{{ month }}</code>, <code>{{ day }}</code>, <code>{{ company_name }}</code>'),

			Field::make( 'text', 'bwp_footer_secondary_colophon_text', 'Secondary colophon text' )
			     ->set_help_text('Use the following dynamic tags to customise the text area: <code>{{ year }}</code>, <code>{{ month }}</code>, <code>{{ day }}</code>, <code>{{ company_name }}</code>')

		) );

			$options = array_merge( $options, array(
				Field::make( 'checkbox', 'bwp_footer_extended', 'Add colophon to separate footer section' )
				     ->set_help_text('adds separate container to the footer for the site attribute text')
				     ->set_width( 50 ),

				Field::make( 'checkbox', 'bwp_footer_colophon_social_links', 'Add social links' )
				     ->set_help_text('adds social links to the colophon')
				     ->set_width( 50 ),
			) );

		return $options;

	}



	/**
	 * Generate the parent theme default header layouts options and apply/modify
	 * the data based on the `bwp_modify_header_options_options` hook
	 *
	 *
	 * @return Field[]
	 */
	public function getHeaderLayoutOptions() {

		$this->headerDefaults();

		$fields = apply_filters( 'bwp_modify_header_options_options', $this->getDefaultHeaderOptions() );

		return $fields;

	}

	/**
	 * Create new args from the $defaultFields for the 'Header one' template
	 *
	 */
	function headerDefaults() {

		// new fields to add
		$new_fields = array(
			array(
				'new_item_key'      => 'bwp_search_icon_position',
				'field'             => Field::make( 'select', 'bwp_search_icon_position', 'Search icon position')
				                            ->add_options( array(
					                            'menu' => 'In primary menu',
					                            'top-right' => 'Top right'
				                            ) )
				                            ->set_width( 66 ),
				'paired_item_key'   => 'bwp_show_search_icon',
				'paired_item_width' => 33
			),
			array(
				'new_item_key'      => 'bwp_menu_style',
				'field'             => Field::make( 'select', 'bwp_menu_style', 'Desktop primary menu style')
				                            ->add_options( array(
					                            'header' => 'In header',
					                            'below-header' => 'Below header',
					                            'off-canvas' => 'Off canvas',
					                            'sticky' => 'Sticky nav',
					                            'sticky-w-logo' => 'Sticky nav w/logo'
				                            ) )
				                            ->set_width( 100 ),
				'paired_item_key'   => 'bwp_header_logo',
			),
			array(
				'new_item_key'      => 'bwp_item_alignment',
				'field'             => Field::make( 'select', 'bwp_item_alignment', 'Align items')
				                            ->add_options( array(
					                            'inline' => 'Inline',
					                            'centered' => 'Centered'
				                            ) ),
				'paired_item_key'   => 'bwp_menu_style'
			),
			array(
				'new_item_key'      => 'bwp_login_icon_position',
				'field'             => Field::make( 'select', 'bwp_login_icon_position', 'Login icon position')
				                            ->add_options( array(
					                            'menu' => 'In primary menu',
					                            'icon-area' => 'Icon area'
				                            ) )
				                            ->set_width( 66 ),
				'paired_item_key'   => 'bwp_show_login_icon',
				'paired_item_width' => 33
			),
//			array(
//				'new_item_key'      => 'bwp_mobile_icon_position',
//				'field'             =>
//					Field::make( 'select', 'bwp_mobile_icon_position', 'Mobile menu icon location' )
//					     ->help_text( 'Append a login icon to the menu bar' )
//					     ->add_options( array(
//						     'top-right' => 'Top right',
//						     'center-right' => 'Center right'
//					     ) )
//					     ->set_width( 100 ),
//				'paired_item_key'   => 'bwp_logo_position'
//			),
			array(
				'new_item_key'      => 'bwp_display_social_icons',
				'field'             =>
					Field::make( 'checkbox', 'bwp_display_social_icons', 'Social icons' )
					     ->help_text( 'Display social icons in the header' )
			),
		);

		// loop array and add items to the
		$this->addFieldsToDefaultFieldOptions( $new_fields );

	}


	/**
	 * Loop new fields and add them to the $defaultFields property
	 *
	 * @param $new_fields
	 */
	private function addFieldsToDefaultFieldOptions( $new_fields ) {


		// loop all fields and add to the default args
		foreach ( $new_fields as $new_field ) :

			$field = isset( $new_field['field'] ) ? $new_field['field'] : false;
			$paired_item_key = isset( $new_field['paired_item_key'] ) ? $new_field['paired_item_key'] : false;
			$new_item_key = isset( $new_field['new_item_key'] ) ? $new_field['new_item_key'] : false;
			$new_paired_width = isset( $new_field['paired_item_width'] ) ? $new_field['paired_item_width'] : false;



			$this->addFieldToDefaultFieldOptions(
				$field,
				$paired_item_key,
				$new_item_key,
				$new_paired_width
			);

		endforeach;
	}

	/**
	 * Insert a new field option to the end of the array or after a relevant field in the default options if the key
	 * of a default option is supplied
	 *
	 * @param Field $field              (required) New field item to be added to the default args
	 * @param bool $paired_item_key     (optional) The key of the item the new field will be placed after in the layout
	 * @param bool $new_key             (optional) The corresponding key of the new field
	 * @param bool $new_width_paired    (optional) The new width of the paired field
	 */
	private function addFieldToDefaultFieldOptions( $field, $paired_item_key = false, $new_key = false ,$new_width_paired = false ) {

		$default_fields = $this->getDefaultHeaderOptions();

		$key_pos = get_key_pos( $paired_item_key, $default_fields );

		// if paired item key is defined, add at location
		if ( $key_pos !== false && $paired_item_key ) :

			// insert is pos
			$default_fields = add_item_after_pos( $field, $key_pos, $default_fields, $new_key );

			// modify paired element width
			if ( is_int( $new_width_paired ) ) :
				$default_fields[$paired_item_key] = $default_fields[$paired_item_key]->set_width( $new_width_paired );
			endif;

		//.. otherwise add to the end of the array
		elseif ( $new_key && !$paired_item_key) :
			$default_fields[$new_key] = $field;
		endif;

		$this->setDefaultFields( $default_fields );

	}



}