<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 15/09/2018
 * Time: 13:06
 */

namespace BespokeParent\Options;


use BespokeParent\Options\Abstracts\SlickOptions;
use Carbon_Fields\Container as Container;
use Carbon_Fields\Field as Field;

class CarouselOptions extends SlickOptions {

	/**
	 * init the container for the slider options
	 */
	protected function createOptionsContainer() {

		// create the container
		$container = Container::make( 'post_meta', 'Config' )
		                      ->where( 'post_type', '=', 'bwp_carousel' );

		$this->setContainer( $container );
	 }


	/**
	 * add the fields for the slider options
	 */
	 protected function addFields() {

		$container = $this->getContainer();

		// Default options (used in general and responsive)
		$slick_options = apply_filters( 'bwp_slick_options', $this->getDefaultSlickOptions() );

		/* General */
			// additional fields for the 'General' fields
			$general_fields = array(
				'bwp_slides_section' =>
					Field::make( 'separator', 'bwp_slides_section', 'Images' ),
				'bwp_slider_image' =>
					Field::make( 'media_gallery', 'bwp_slider_image', '' )
						->set_type( array( 'image' ) )
						->set_help_text( 'add all the required images here that need to appear in the carousel' ),
			);


		/* Responsive */
			// repeater field for the 'Responsive' options
			$responsive_options = $this->getResponsiveDefaultSlickOptions();


		 // new labels for the media repeater field
		 $slider_labels = array(
			 'plural_name' => 'slide sets',
			 'singular_name' => 'slide set',
		 );


		 /* Add fields to container */
			$container
				->add_tab( 'Carousel', array(
					Field::make( 'complex', 'bwp_slider' )
					     ->add_fields( 'image_slider', $general_fields )
					     ->add_fields( 'testimonial_slider', $this->getTestimonialOptions() )
					     ->add_fields( 'post_slider', $this->getPostOptions() )
					     ->setup_labels( $slider_labels )
					     ->set_min(1)
					     ->set_max(1)
				) )
				->add_tab( 'Options', $slick_options )
				->add_tab( 'Responsive', $responsive_options );

	}


	private function getTestimonialOptions() {


		// new labels for the media repeater field
		$testimonial_labels = array(
			'plural_name' => 'Testimonials',
			'singular_name' => 'slide',
		);

		$testimonial_fields = array(
			Field::make('complex', 'bwp_testimonials', '')
			     ->add_fields( array(
				     Field::make('text', 'bwp_testimonial_name', 'Author')
				          ->set_width(50)
				          ->set_help_text( 'The name of the author of the testimonial' ),
				     Field::make('date', 'bwp_testimonial_date', 'Date received')
				          ->set_width(50)
				          ->set_help_text( 'When the review was dated (optional)' ),
				     Field::make('rich_text', 'bwp_testimonial_review', 'Testimonial'),
			     ) )
			     ->setup_labels( $testimonial_labels )
			     ->set_layout( 'tabbed-horizontal' )
		);

		return $testimonial_fields;

	}


	private function getPostOptions() {

		// new labels for the media repeater field
		$testimonial_labels = array(
			'plural_name' => 'Posts',
			'singular_name' => 'slide',
		);

		$testimonial_fields = array(
			Field::make( 'association', 'bwp_post_slides', __( 'Posts to show' ) )
		);

		return $testimonial_fields;

	}

}