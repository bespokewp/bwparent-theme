<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 16/09/2018
 * Time: 19:31
 */

namespace BespokeParent\Options;


use BespokeParent\Options\Abstracts\SlickOptions;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class HeaderSliderOptions extends SlickOptions {


	/**
	 * @see SlickOptions::createOptionsContainer()
	 */
	protected function createOptionsContainer() {
		$container = Container::make( 'post_meta', 'Header slider' )
		                      ->where( 'post_type', '=', 'page' );

		$this->setContainer( $container );
	}


	/**
	 * add the fields for the slider options
	 */
	protected function addFields() {

		$container = $this->getContainer();

		$slick_options = $this->getDefaultSlickOptions();
		$header_fields = $this->getHeaderSliderOptions();


		/* Add fields to container */
		$container
			->add_tab( 'Slides', $header_fields )
			->add_tab( 'Defaults', $slick_options )
			->add_tab( 'Responsive', $this->getResponsiveDefaultSlickOptions() );

	}

	/**
	 * custom header slider options
	 */
	private function getHeaderSliderOptions() {


		// new labels for the media repeater field
		$header_labels = array(
			'plural_name' => 'Slides',
			'singular_name' => 'Slide',
		);

		$header_fields = array(
			Field::make('complex', 'bwp_header_slides', '')
			     ->add_fields( array(
				     Field::make('text', 'bwp_header_slides_title', 'Title')
				          ->set_width(100)
				          ->set_help_text( 'The name of the author of the testimonial' ),

				     Field::make('rich_text', 'bwp_header_slides_content', 'Content')
				          ->set_help_text( 'Any additional markup to include in the slider' ),


				     Field::make('separator', 'bwp_separator_cta', 'CTAs'),
				     Field::make('complex', 'bwp_header_ctas', '')
			            ->add_fields( array(

				            Field::make('text', 'bwp_header_slides_button_text', 'Text')
				                 ->set_help_text( 'text to go in the CTA button' )
				                 ->set_width(50),

				            Field::make('text', 'bwp_header_slides_button_link', 'Link')
				                 ->set_help_text( 'add a URL here for the CTA button to link to when clicked' )
				                 ->set_width(50),

				            Field::make('select', 'bwp_header_cta_target', 'Open link in..')
				                 ->add_options(
					                 array(
						                 'same' => 'Same window',
						                 'new-tab' => 'New tab'
					                 )
				                 )
				                 ->set_help_text( 'Add a custom css class here for this CTA' )
				                 ->set_width(50),

				            Field::make('text', 'bwp_header_cta_custom_class', 'Custom CSS class')
				                 ->set_help_text( 'Add a custom css class here for this CTA' )
				                 ->set_width(50),
			            ) )
			            ->set_layout( 'tabbed-horizontal' )
			            ->set_header_template( '
							<% if ( bwp_header_slides_button_text ) { %>
							        <%- bwp_header_slides_button_text %>
							    <% }  else { %>
							        custom CTA
							    <% } %>
							' ),

				     Field::make('separator', 'bwp_separator_styling', 'Styling'),

				     Field::make('color', 'bwp_header_slides_text_color', 'Text color')
				          ->set_help_text( 'Choose a color for the slider text' ),

				     Field::make('color', 'bwp_header_slides_color', 'Background color')
				          ->set_help_text( 'Choose a color for the slider background' )
				          ->set_width(50),

				     Field::make('text', 'bwp_header_slides_opacity', 'Background color opacity percent')
				          ->set_help_text( 'Choose a color for the slider background' )
					      ->set_attributes( array(
					      	  'min' => 0,
					          'max' => 100
					      ) )
					      ->set_width(50),

				     Field::make('image', 'bwp_header_slides_image', 'Background image')
				          ->set_help_text( 'Add an image to the slide background' ),



			     ) )
			     ->setup_labels( $header_labels )
			     ->set_layout( 'tabbed-horizontal' )
		);

		return $header_fields;

	}

}