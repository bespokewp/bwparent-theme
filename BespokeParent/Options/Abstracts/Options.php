<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 18/09/2018
 * Time: 20:18
 */

namespace BespokeParent\Options\Abstracts;
use Carbon_Fields\Container;


abstract class Options {

	protected $container;

	private $module_name;


	/**
	 * Create a container for options to be added to
	 */
	abstract protected function createOptionsContainer();


	/**
	 * Add the fields to the container to be displayed in the admin screen
	 */
	abstract protected function addFields();

	public function __construct( $module_name = false ) {

		$this->setModuleName( $module_name );

		// if no module is required
		if ( $module_name ) :

			add_action( 'carbon_fields_fields_registered', function() {

				$module_name = $this->getModuleName();
				$module = cto( $module_name );

				if ( $module ) :
					$this->createOptionsContainer();
					$this->addFields();
				endif;

			}, 5 );

		// check module is active
		else :

			add_action( 'after_setup_theme', function() {
				$this->createOptionsContainer();
				$this->addFields();
			});


		endif;

	}

	/**
	 * @return Container
	 */
	protected function getContainer() {
		return $this->container;
	}

	/**
	 * @param Container $container
	 */
	protected function setContainer( $container ): void {
		$this->container = $container;
	}

	/**
	 * @return string|bool
	 */
	public function getModuleName() {
		return $this->module_name;
	}

	/**
	 * @param string|bool $module_name
	 */
	public function setModuleName( $module_name ): void {
		$this->module_name = $module_name;
	}



}