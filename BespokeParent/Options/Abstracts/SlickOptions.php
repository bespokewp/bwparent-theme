<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 16/09/2018
 * Time: 11:05
 */

namespace BespokeParent\Options\Abstracts;

use Carbon_Fields\Field;

abstract class SlickOptions extends Options {

	protected function getDefaultSlickOptions( $slug = '' ) {

		return array(
			'bwp_controls_section' . $slug =>
				Field::make( 'separator', 'bwp_controls_section' . $slug, 'Controls' ),

			'bwp_arrows' . $slug =>
				Field::make( 'checkbox', 'bwp_arrows' . $slug, 'Add arrow controls' )
					->help_text( 'tick this box to display arrow controls on the carousel' )
					->set_width(50),

			'bwp_dots' . $slug =>
				Field::make( 'checkbox', 'bwp_dots' . $slug, 'Add dot controls' )
					->help_text( 'tick this box to display dot controls on the carousel' )
					->set_width(50),


			'bwp_presentation_section' . $slug =>
				Field::make( 'separator', 'bwp_presentation_section' . $slug, 'Presentation' ),

			'bwp_speed' . $slug =>
				Field::make( 'text', 'bwp_speed' . $slug, 'Carousel speed' )
					->help_text( 'auto-carousel speed in milliseconds. 0 will turn off auto-scroll' )
					->set_width(50),

			'bwp_slides_to_show' . $slug =>
				Field::make( 'text', 'bwp_slides_to_show' . $slug, 'Slides to show' )
					->help_text( 'How many carousel to show in each scroll' )
					->set_attribute( 'type', 'number' )
					->set_attribute( 'min', 0 )
					->set_width(50),
		);

	}

	protected function getResponsiveDefaultSlickOptions() {

		$additional_field = array (
			'bwp_slick_media_size' =>
				Field::make( 'text', 'bwp_slick_media_size', 'Max screen width (pixles)' )
					->help_text( 'Add the max width (in pixels) that these options will be active on' )
					->set_width(100)
		);

		$media_options = array_merge( $additional_field, $this->getDefaultSlickOptions( '_responsive' ) );

		return array(
			Field::make('complex', 'bwp_media_options', 'Custom screen size options')
				->add_fields( $media_options )
				->setup_labels( array(
					'plural_name' => 'Responsive options',
					'singular_name' => 'Responsive options',
				) )
				->set_layout( 'tabbed-horizontal' )

		);
	}

}