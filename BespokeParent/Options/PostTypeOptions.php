<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 06/11/2018
 * Time: 15:08
 */

namespace BespokeParent\Options;


use Carbon_Fields\Container;
use Carbon_Fields\Field\Field;

class PostTypeOptions {

	public function __construct() {

		add_action( 'carbon_fields_register_fields', array( $this, 'portfolio_options' ) );
	}

	/**
	 * Carbon Fields theme options container creation
	 */
	public function portfolio_options() {

		$container = Container::make( 'post_meta', __( 'Additional info', 'crb' ) )
			->where('post_type', '=', 'bwp_portfolio')
			->set_context( 'normal' );

		$container->add_fields( array(
			Field::make( 'text', 'portfolio_link', 'Link to site' )
		) );
	}

}