<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 29/09/2018
 * Time: 15:29
 */

namespace BespokeParent\Options;


use BespokeParent\Options\Abstracts\Options;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

class DelegatesOptions extends Options {

	/**
	 * init the container for the slider options
	 */
	protected function createOptionsContainer() {

		// create the containers
		$container = Container::make( 'post_meta', 'Team details' )
		                      ->where( 'post_type', '=', 'bwp_teams' );

		$this->setContainer( $container );
	}

	/**
	 * add the fields for the slider options
	 */
	protected function addFields() {

		$container = $this->getContainer();

		$slick_options = $this->getDelegateOptions();

		/* Add fields to container */
		$container
			->add_tab( 'Delegates', $slick_options );

	}

	/**
	 * Default options for the delegate information
	 * @return array
	 */
	private function getDelegateOptions() {

		return array(

			Field::make('complex', 'bwp_delegates_info')

				->add_fields(
					// custom filter for adding/removing carbon fields options
					apply_filters( 'bwp_modify_delegate_options', array(


							Field::make('image', 'bwp_delegate_photo', 'Photo')
							     ->set_width( 100 ),

							Field::make('text', 'bwp_delegate_name', 'Name')
							     ->set_width( 50 ),

							Field::make('text', 'bwp_delegate_position', 'Position')
							     ->set_width( 50 ),

							Field::make('text', 'bwp_delegate_email', 'Email')
								->set_attributes( array(
									'type' => 'email'
								) )
								->set_width( 50 ),

							Field::make('text', 'bwp_delegate_work_number', 'Work number')
								->set_width( 50 ),

							Field::make('rich_text', 'bwp_delegate_bio', 'Bio'),

						)
					)
				)
				->set_layout( 'tabbed-horizontal' )
				->set_header_template( '
					<% if ( bwp_delegate_name ) { %>
					        <%- bwp_delegate_name %>
					    <% } else { %>
					        Delegate
					    <% } %>
					' )

		);

	}


}