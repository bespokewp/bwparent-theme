<?php
/**
 * Created by PhpStorm.
 * User: liammaclachlan
 * Date: 15/09/2018
 * Time: 15:44
 */

namespace BespokeParent;


class EnqueueScripts {


	public function __construct() {


		// frontend
		add_action('wp_enqueue_scripts', function() {


			/** Library style */
			// Bootstrap styles
			wp_enqueue_style( 'frameworks', fsi( get_template_directory_uri() ) . 'assets/frontend/css/libraries.min.css' );

			/** Theme styles */
			// styles
			wp_enqueue_style( 'parent_styles', fsi( get_template_directory_uri() ) . 'style.css', array('frameworks') );


			/** Theme scripts */
			// scripts
			wp_enqueue_script( 'parent_scripts', fsi( get_template_directory_uri() ) . 'assets/frontend/js/scripts.min.js', array('jquery') );

		}, 5);


		// frontend
		add_action('admin_enqueue_scripts', function() {


			/** Theme styles/scripts */
			// styles
			wp_enqueue_style( 'theme_styles', fsi( get_template_directory_uri() ) . 'assets/css/admin/admin.css' );


		});

	}

}


?>

