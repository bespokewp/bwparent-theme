<?php

/***
 * Theme Auto-loader (for local classes)
 ***/
	spl_autoload_register(function ($class) {

		// base directory for the namespace prefix
		$base_dir = __DIR__ . '/';


		// replace the namespace prefix with the base directory, replace namespace
		// separators with directory separators in the relative class name, append
		// with .php
		$file = $base_dir . str_replace('\\', '/', $class) . '.php';

		// if the file exists, require it
		if ( file_exists($file) )
			require $file;

	});

/**
 * File includes
 */
require('parts/utility.php');
require('parts/theme-funcs.php');

/**
 * Init classes
 */

	use Carbon_Fields\Carbon_Fields;
	use BespokeParent\Options\ThemeOptions;
	use BespokeParent\Features\HeaderSlider;


/**
 * Init and run Carbon fields
 */
	function crb_load() {
		// for some reason, won't run unless it's in the function.php (even when modifying the rel path)
		require_once( 'vendor/autoload.php' );
		Carbon_Fields::boot();
		new ThemeOptions();
		new BespokeParent\Options\CarouselOptions( 'bwp_carousel_module' );
		new BespokeParent\Options\HeaderSliderOptions( 'bwp_header_slider_module' );
		new BespokeParent\Options\DelegatesOptions( 'bwp_teams_module' );
		new BespokeParent\Options\PostTypeOptions();

		// add additional options using this hook to guarantee the auto-loader has completed
		do_action('after_carbon_init' );

	}
	add_action( 'after_setup_theme', 'crb_load', 5 );


    add_action( 'init', function() {

        /**
         * Run theme init classes
         */
        // Standard
        new BespokeParent\ShortCodes();

    });

    // Standard
    new BespokeParent\PostTypes();
    new BespokeParent\EnqueueScripts();
    new BespokeParent\Widgets();
    new BespokeParent\ThemeSetup();

/**
 * Test bed
 */

add_action('wp_head', 'add_header_slider');
function add_header_slider() {

	global $post;

	if ( isset ( $post->ID ) || is_home() ) :


		if ( is_home() )
			$post = get_post( get_option( 'page_for_posts' ) );

        new HeaderSlider( $post->ID );

        $header_slides = carbon_get_post_meta($post->ID, 'bwp_header_slides');

        if ( !empty( $header_slides ) ) :

            ob_start(); ?>

            <style> <?php

            foreach ( $header_slides as $key => $header_slide ) : ?>

                .slider-<?php echo $key; ?> {

                    <?php  if (
                            !empty( $header_slide['bwp_header_slides_color'] )
                            && empty( $header_slide['bwp_header_slides_image'] )
                        ) :

                            $hex = $header_slide['bwp_header_slides_color'];
                            list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

                            $opacity = !empty(  $header_slide['bwp_header_slides_opacity'] )
                                ? round( $header_slide['bwp_header_slides_opacity'] ) > 0
                                    ? round( $header_slide['bwp_header_slides_opacity'] ) / 100
                                    : 0
                                : 1; ?>

                            background: rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>); /* Old browsers */
                            background: -moz-linear-gradient(top, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 0%, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 100%); /* FF3.6-15 */
                            background: -webkit-linear-gradient(top, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 0%,rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 100%); /* Chrome10-25,Safari5.1-6 */
                            background: linear-gradient(to bottom, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 0%,rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 100%);


                        <?php elseif (
                            !empty( $header_slide['bwp_header_slides_color'] )
                            && !empty( $header_slide['bwp_header_slides_image'] )
                        ) :

                            $hex = $header_slide['bwp_header_slides_color'];
                            list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");

                            $opacity = !empty(  $header_slide['bwp_header_slides_opacity'] )
                                ? round( $header_slide['bwp_header_slides_opacity'] ) > 0
                                    ? round( $header_slide['bwp_header_slides_opacity'] ) / 100
                                    : 0
                                : 1; ?>

                            background: rgb(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>); /* Old browsers */

                            background: -moz-linear-gradient(top, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 0%, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 100%), url(<?php echo wp_get_attachment_url( $header_slide['bwp_header_slides_image'] )?>);

                            background: -webkit-linear-gradient(top, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 0%,rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 100%), url(<?php echo wp_get_attachment_url( $header_slide['bwp_header_slides_image'] )?>);

                            background: linear-gradient(to bottom, rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 0%,rgba(<?php echo $r; ?>,<?php echo $g; ?>,<?php echo $b; ?>,<?php echo $opacity; ?>) 100%), url(<?php echo wp_get_attachment_url( $header_slide['bwp_header_slides_image'] )?>);

                        <?php elseif (
                            empty( $header_slide['bwp_header_slides_color'] )
                            && !empty( $header_slide['bwp_header_slides_image'] )
                        ) : ?>
                            background: url(<?php echo wp_get_attachment_url( $header_slide['bwp_header_slides_image'] )?>);
                        <?php endif; ?>

                    <?php if ( !empty( $header_slide['bwp_header_slides_text_color'] ) ) : ?>
                        color: <?php echo $header_slide['bwp_header_slides_text_color']; ?>;
                    <?php endif; ?>

                    background-repeat: no-repeat;
                    background-size: cover;

                } <?php

            endforeach; ?>

            </style> <?php

            echo ob_get_clean();

        endif;

	endif;

}

////////////////
// Block rendering
////////
function render_portfolio() {
    get_template_part('templates/content/portfolio-blocks');
}


function render_company_details() {
    get_template_part('templates/info/company', 'details');
}

/////
// simple conditional checks
////
function check_post_count( $post_type = 'posts' ) {
    $portfolio_args = array(
    	'post_type'  => $post_type,
    	'posts_per_page' => -1,
    );

    $portfolio_posts = new WP_Query( $portfolio_args );

    $post_count = $portfolio_posts->post_count;

    wp_reset_postdata();

    return $post_count;
}