        <footer>

            <div class="footer__outer-container">

                <div class="footer__inner-container container">

                <?php

                $footer_widget_count = is_numeric( carbon_get_theme_option('bwp_footer_widgets_total') ) ? (int)carbon_get_theme_option('bwp_footer_widgets_total') : 0;
                $i = 1;

                if ( is_int($footer_widget_count) && $footer_widget_count >= 1 ) : ?>

                        <div class="widgets__container row">

                            <?php while ( $i <= $footer_widget_count ) : ?>

                                <div class="widget__container">

                                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footer-area-" . $i ) ) : ?>

                                        Missing footer widget area <?php echo $i; ?>

                                    <?php endif;

                                    $i++; ?>

                                </div>

                            <?php endwhile; ?>

                        </div>

                <?php endif; ?>

                <?php if ( !carbon_get_theme_option('bwp_footer_extended') ) : ?>
                    <div class="row">
                        <div class="col-12">
				            <?php get_template_part('templates/footer/colophon', 'main'); ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

            </div>


	        <?php if ( carbon_get_theme_option('bwp_footer_extended') ) : ?>
                <div class="footer__colophon-container container">
                    <div class="row">
                        <div class="col-12">
					        <?php get_template_part('templates/footer/colophon', 'main'); ?>
                        </div>
                    </div>
                </div>
	        <?php endif; ?>


        </footer>
        <?php wp_footer(); ?>
    </body>
</html>